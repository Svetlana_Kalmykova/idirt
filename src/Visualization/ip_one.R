require(data.table)
library(ggpubr)
library(reshape2)
library(grid)
source("lib/helper_functions.R")

PlotIPIntensityDistribution <- function(idirt.mix, idirt.ip, 
                                        experiment.name.mix, experiment.name.ip, 
                                             color.list = c(scales::alpha("darkred", 0.7),
                                                            scales::alpha("darkgrey", 0.7)),
                                             percentile = 0.99) {


  col.name.mix <- paste0("Ratio H/L ", experiment.name.mix)
  col.name.ip <- paste0("Ratio H/L ", experiment.name.ip)

  intensities.no.na.mix <- 
    idirt.mix$GetProteinGroupsIntensityTable()[
      !(is.nan(idirt.mix$GetProteinGroupsIntensityTable()[[col.name.mix]])), ]
  intensities.no.na.ip <- 
    idirt.ip$GetProteinGroupsIntensityTable()[
      !(is.nan(idirt.ip$GetProteinGroupsIntensityTable()[[col.name.ip]])), ]
  
  intensities.no.na.mix$Score <- as.numeric(unlist(intensities.no.na.mix[, col.name.mix, with = F] / (1 + intensities.no.na.mix[, col.name.mix, with = F]))) * 100
  intensities.no.na.ip$Score <- as.numeric(unlist(intensities.no.na.ip[, col.name.ip, with = F] / (1 + intensities.no.na.ip[, col.name.ip, with = F]))) * 100

  
  percentile.threshold <- quantile(intensities.no.na.mix$Score, probs = percentile)
  
  n.sign.proteins <- nrow(intensities.no.na.ip[intensities.no.na.ip$Score > percentile.threshold, ])
  
  merged <- merge(intensities.no.na.mix[, c('Score', 'GeneBest')], 
                  intensities.no.na.ip[, c('Score', 'GeneBest')], 
                  all = T, by = 'GeneBest', suffixes = c('_mix', '_ip'))
  merged <- melt(merged, id.vars = 'GeneBest')
  colnames(merged) <- c('Gene', 'experiment', 'Score')
  merged$experiment <- gsub("Score_", "", merged$experiment)
  
  png(paste0("out/", MakeDate(), "_protein_dist_", experiment.name, ".png"),  
      width = 7, height = 7, units = 'in', res = 300)
  
  
  
  
  ggdensity(merged, x = "Score", fill = 'experiment', 
            palette = color.list, 
            label = "Gene", repel = T, label.select = list(criteria = "`Score` > percentile.threshold & `experiment` == 'ip'")) +
    geom_vline(xintercept = percentile.threshold, linetype = 'dashed', color = 'grey', size = 1) +
    geom_text(aes(x = percentile.threshold, y = max(density.proteins.mix$y) * 0.9,
                  label = paste("Mix ", percentile * 100, "%\n", sep = '')), 
              angle = 90) +
    geom_text(aes(x = percentile.threshold, y = max(density.proteins.mix$y) * 0.9,
                  label = paste("\n#sign. proteins = ", n.sign.proteins, sep = '')),
              angle = 90) + 
    scale_fill_manual(labels =  paste(c("IP", "Mix"), 
                                        "\n#proteins = ",
                                        c(dim(intensities.no.na.ip)[1], dim(intensities.no.na.mix)[1]), 
                                        "\nmedian = ",
                                        c(paste(round(median(intensities.no.na.ip$Score), 1), sep = ''), 
                                          round(median(intensities.no.na.mix$Score), 1)),
                                        c("\n"), sep = ''), 
                      values = color.list) +
    theme(legend.justification = c(0, 0), 
          legend.position = c(0.01, 0.5), 
          legend.title = element_blank(), 
          legend.key.size = unit(0.5, 'in'), 
          legend.text = element_text(size = 13)) +
    ggtitle(label = str_split_fixed(experiment.name.ip, '_', Inf)[, 3]) +
    theme(plot.title = element_text(size = 22, hjust = 0.5))
    xlab(label = "% of heavy-labeled")
    

  
  
  
  
  
  
  
  
  
  
  
  
  
  dev.off()
}



