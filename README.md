This is a repository for Chart module inside Macromolecule charting framework

This project is continious merge of 3 ongoing projects 
1 
idirt - processing i-dirt data
https://bitbucket.org/Svetlana_Kalmykova/idirt/src/master/
2
line-1 - processing immuniprecipitation data
https://bitbucket.org/altukhov/line-1/src/master/
3
processing whole proteome data without pull-outs
https://bitbucket.org/Svetlana_Kalmykova/ncbp/src/master/

three projects above are being merged into Chart module -
a collection of tools that analyse and ultimately
integrate multiple kinds of molecular data. To date
we have used both label-free and metabolically
labeled mass spectrometry data. First, data are
processed with a mass spectrum search engine to

assign peptides to proteins. Chart accepts a multi-
dimensional input of protein intensities under

many experimental frameworks and applies e.g.
normalizations, clustering, and dimensionality
reduction (under different parameters depending
upon the data) to provide an output that reveals
co-behavioral patterns between constituents of
macromolecules. Chart assembles and reveals
these differences using a wide range of
visualizations. These provide the basis of
biological understanding and the means for
hypothesis development and/or testing. Because
our wet lab research approach intrinsically
surveys dozens of experimental conditions by
default during the exploration phase, plus
additional boutique ad hoc experiments to probe
specific hypotheses - Chart allows us to extract
maximum value from the effort.

